from flask import Flask

app = Flask(__name__)
from controller import *


@app.route('/')
def welcome():
    return "Hello Flask World !!!"


@app.route("/home")
def home():
    return "You are in flask home page !"
