from app import app
from model.user_model import UserModel
from flask import request
from datetime import datetime

obj = UserModel()


@app.route("/user/getall")
def signup():
    return obj.user_getall_model()


@app.route("/user/addone", methods=['POST'])
def user_post_controller():
    return obj.user_postone_model(request.json)


@app.route("/user/update", methods=['PUT'])
def user_update_controller():
    return obj.user_update_model(request.json)


@app.route("/user/delete/<id>", methods=['DELETE'])
def user_delete_controller(id):
    return obj.user_delete_model(id)


@app.route("/user/patch/<id>", methods=['PATCH'])
def user_patch_controller(id):
    return obj.user_patch_model(request.json, id)


@app.route("/user/getall/limit/<limit>/page/<page>", methods=['GET'])
def user_pagination_controller(limit, page):
    return obj.user_pagination_model(limit, page)


@app.route("/user/<uid>/upload/avatar", methods=['PUT'])
def user_upload_avatar(uid):
    file = request.files['avatar']
    uniqueFileName = str(datetime.now().timestamp()).replace(".", "")
    fileNameSplit = file.filename.split(".")
    ext = fileNameSplit[len(fileNameSplit) - 1]
    finalFilePath = f"uploads/{uniqueFileName}.{ext}"
    file.save(finalFilePath)
    return obj.user_avatar_model(uid, finalFilePath)
