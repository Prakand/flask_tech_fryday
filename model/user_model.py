import json

import mysql.connector
from flask import make_response
import status


class UserModel():
    def __init__(self):
        try:
            self.con = mysql.connector.connect(host='localhost', user='root', password='Mishra@2001',
                                               database='flask_techfry')
            self.con.autocommit = True
            self.cur = self.con.cursor(dictionary=True)
            print("Connection Successful !!")
        except:
            print("Connection Failed !!!")

    def user_getall_model(self):
        self.cur.execute("SELECT * FROM users")
        result = self.cur.fetchall()
        if len(result) > 0:
            return make_response({"payload": result}, status.HTTP_200_OK)
        else:
            return make_response({"message": "No data found"}, status.HTTP_204_NO_CONTENT)

    def user_postone_model(self, data):
        self.cur.execute(
            f"INSERT INTO users(name, email, phone, role, password) VALUES('{data['name']}', '{data['email']}', '{data['phone']}', '{data['role']}', '{data['password']}')")
        return make_response({"message": "user created successfully"}, status.HTTP_201_CREATED)

    def user_update_model(self, data):
        self.cur.execute(
            f"UPDATE users SET name='{data['name']}', email='{data['email']}', phone='{data['phone']}', role='{data['role']}', password='{data['password']}' WHERE id={data['id']} ")
        if self.cur.rowcount > 0:
            return make_response({"message": "user updated successfully"}, status.HTTP_201_CREATED)
        else:
            return make_response({"message": "No change happen"}, status.HTTP_202_ACCEPTED)

    def user_delete_model(self, id):
        self.cur.execute(f"DELETE FROM users WHERE id={id}")
        if self.cur.rowcount > 0:
            return make_response({"message": "user deleted successfully"}, status.HTTP_200_OK)
        else:
            return make_response({"message": "No change reflect"}, status.HTTP_204_NO_CONTENT)

    def user_patch_model(self, data, id):
        qry = "UPDATE users SET "
        for key in data:
            qry += f"{key}='{data[key]}',"
        qry = qry[:-1] + f" WHERE id={id}"

        self.cur.execute(qry)

        if self.cur.rowcount > 0:
            return make_response({"message": "Updated Successfully"}, status.HTTP_200_OK)
        else:
            return make_response({"message": "Nothing to update"}, status.HTTP_202_ACCEPTED)

    def user_pagination_model(self, limit, page):
        limit = int(limit)
        page = int(page)
        start = (limit * page) - limit
        qry = f"SELECT * FROM users LIMIT {start}, {limit}"
        self.cur.execute(qry)
        result = self.cur.fetchall()
        if len(result) > 0:
            return make_response({"payload": result, "page_no": page, "limit": limit}, status.HTTP_200_OK)
        else:
            return make_response({"message": "No Data Found"}, status.HTTP_204_NO_CONTENT)

    def user_avatar_model(self, uid, filepath):
        self.cur.execute(f"UPDATE users SET avatar='{filepath}' WHERE id={uid}")
        if self.cur.rowcount > 0:
            return make_response({"message": "User Updated successfully"}, status.HTTP_200_OK)
        else:
            return make_response({"message": "Nothing to Update"}, status.HTTP_202_ACCEPTED)
